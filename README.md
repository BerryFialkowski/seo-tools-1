# What is this?

This part of SEO tool. It is about retrieving google suggestions for given keyword.

# Quick start

For really quick start you need you need to have `docker` and `git`.

    $ git clone https://outoftime@bitbucket.org/outoftime/seo-tools.git ./
    $ git checkout docker-solution
    $ docker build -t my-images/seo-tools-suggestions .
    $ docker run -d -p 8000:80 \
                    --name scraper \
                    -v $(pwd)/src:/home/app/scraper \
                    my-images/seo-tools-suggestions
    $ docker exec scraper bash post_install.sh
    $ docker restart scraper
    $ docker inspect -f '{{ .NetworkSettings.IPAddress }}' scraper

To use authentication sqlite database required

    $ docker exec scraper python db_create.py

Then you need to create user, type following command

    $ docker exec scraper python db_create_user.py <USERNAME> <PASSWORD>

where `<USERNAME>`, `<PASSWORD>` - authentication credentials

Also download static files with help of `bower`

    $ bower install

Open link in you favorite browser [http://localhost:8000][1]

[1]: http://localhost:8000
