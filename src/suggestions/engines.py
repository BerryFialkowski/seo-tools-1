# ~*~ encoding: utf-8 ~*~
import os

from ConfigParser import ConfigParser
from parsers import GoogleSuggestionParser


proxy_config = ConfigParser(allow_no_value=True)
ROOT = os.path.dirname(os.path.realpath(__file__))
proxy_config.read(os.path.join(ROOT, 'proxies.cfg'))


class Engine(object):
    """
    http://willdrevo.com/using-a-proxy-with-a-randomized-user-agent-in-python-requests/
    """

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                      '(KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36'
    }
    method = 'get'
    parser = None

    def get_params(self, keyword):
        raise NotImplemented

    def get_proxies(self):
        proxies = dict()
        if proxy_config.has_section('url'):
            for i, protocol in enumerate(['http', 'https']):
                if proxy_config.has_option('url', protocol):
                    proxies[protocol] = proxy_config.get('url', protocol)
        return proxies

    def get_suggestions(self, page):
        return self.parser.parse(page)

    def get_url(self, keyword):
        raise NotImplemented


class GoogleSuggestionEngine(Engine):

    parser = GoogleSuggestionParser()

    def get_params(self, keyword):
        return dict(output='toolbar', hl='en', q=keyword)

    def get_url(self, keyword):
        return 'http://suggestqueries.google.com/complete/search'
