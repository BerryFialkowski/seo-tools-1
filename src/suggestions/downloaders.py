# ~*~ encoding: utf-8 ~*~
import requests


class Downloader(object):

    def load(self, keyword, engine):
        raise NotImplemented


class PhantomJSDownloader(Downloader):

    pass


class RequestsDownloader(Downloader):

    def load(self, keyword, engine):
        request = None
        if engine.method == 'get':
            request = requests.get
        response = request(engine.get_url(keyword),
                           headers=engine.headers,
                           params=engine.get_params(keyword),
                           proxies=engine.get_proxies())
        return response.text
