# ~*~ encoding: utf-8 ~*~
import logging
import os
from random import randint
import time

from db import RedisDatabase
from scraper import Scraper
from tasks import Task
from threading import Thread


LOGFILE_PATH = '/var/log/scraper/'

if not os.path.exists(LOGFILE_PATH):
    os.mkdir(LOGFILE_PATH)

logging.basicConfig(
    filename=os.path.join(LOGFILE_PATH, 'workers.log'), level=logging.INFO,
    format='%(asctime)s %(message)s')


class Worker(Thread):

    daemon = True

    def _create_task(self, keyword, sibling, depth, with_numbers):
        if depth > 0:
            Task.create_task_suggestion(keyword, sibling, depth, with_numbers)

    def run(self):
        scraper = Scraper()
        while True:
            task = Task.get_task()
            keyword, depth, with_numbers = task['keyword'], \
                int(task['depth']), bool(task['with_numbers'])

            # logging.info('Worker "{}" accept task for keyword "{}"'
            #              .format(self.name, keyword))

            for sibling in scraper.get_keyword_siblings(
                    keyword, with_numbers):
                self._create_task(keyword, sibling, depth, with_numbers)

            # logging.info('Worker "{}" complete task for keyword "{}"'
            #              .format(self.name, keyword))


class SuggestionWorker(Thread):

    daemon = True

    def _create_suggestion(self, keyword, suggestion):
        rdb = RedisDatabase.instance()
        rdb.add_suggestion(keyword, suggestion)

    def _create_task(self, suggestion, depth):
        if depth > 1:
            Task.create_task(suggestion, depth - 1)

    def run(self):
        scraper = Scraper()
        while True:
            task = Task.get_task_suggestion()
            keyword, sibling, depth, with_numbers = task['keyword'], \
                task['sibling'], int(task['depth']), bool(task['with_numbers'])

            # logging.info('Worker "{}" accept task for keyword "{}"'
            #              .format(self.name,
            #                      [keyword, sibling, depth, with_numbers]))

            suggestions = scraper.scrape_suggestions(sibling)

            if not suggestions:
                logging.error('Cannot scrape suggestions. Check you proxy.')

            for suggestion in suggestions:
                self._create_suggestion(keyword, suggestion)
                self._create_task(suggestion, depth)

            # logging.info('Worker "{}" complete task for keyword "{}" with {} suggestions'
            #              .format(self.name, keyword, suggestions))

            time.sleep(randint(2, 4))


class WorkerManager(object):

    def __init__(self, number_of_workers=1):
        self.number_of_workers = number_of_workers
        self.workers = [SuggestionWorker(name='suggestion-worker-{}'.format(i))
                        for i in range(number_of_workers)]
        # self.workers = []
        self.workers.append(Worker(name='worker-0'))

    def start(self):
        for i, worker in enumerate(self.workers):
            worker.start()


if __name__ == '__main__':
    manager = WorkerManager()
    manager.start()
    while True:
        time.sleep(1)
