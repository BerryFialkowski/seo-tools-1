# ~*~ encoding: utf-8 ~*~
import imp

from migrate.versioning import api

from app import db
from app.config import SQLALCHEMY_DATABASE_URI
from app.config import SQLALCHEMY_MIGRATE_REPO


if __name__ == '__main__':
    version = api.db_version(SQLALCHEMY_DATABASE_URI,
                             SQLALCHEMY_MIGRATE_REPO) + 1
    migration = '{0}/versions/{1:04}_migration.py'.format(
        SQLALCHEMY_MIGRATE_REPO, version)

    tmp_module = imp.new_module('old_model')
    old_model = api.create_model(SQLALCHEMY_DATABASE_URI,
                                 SQLALCHEMY_MIGRATE_REPO)
    exec old_model in tmp_module.__dict__
    script = api.make_update_script_for_model(
        SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, tmp_module.meta,
        db.metadata)
    open(migration, "wt").write(script)
    api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

    print('New migration saved as ' + migration)
    print('Current database version: '
          + str(api.db_version(SQLALCHEMY_DATABASE_URI,
                               SQLALCHEMY_MIGRATE_REPO)))
